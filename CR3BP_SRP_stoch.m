function X_dot = CR3BP_SRP_stoch(t, X, mu, C_R_vec, t_vec, Phi, A_sc, c)

    % Reshape to column vector
    X = reshape(X,length(X),[]);

    % Positions
    x = X(1);
    y = X(2);
    z = X(3);

    % Velocities
    xdot = X(4);
    ydot = X(5);
    zdot = X(6);
    
    % Mass
    m = X(7);
    
    % Control
    a_u = X(8:10);

    % Distances to primaries
    r1 = sqrt((x+mu)^2 + y^2 + z^2);
    R_sc = [x; y; z];
    R_sun = [-mu; 0; 0]; % Assuming Sun is first primary, DOES NOT WORK OTHERWISE
    R2 = R_sc - R_sun;
    r2 = sqrt((x-1+mu)^2 + y^2 + z^2);
    
    % Accelerations 
    xddot = 2*ydot + x -(1-mu)*((x+mu)/(r1^3)) - mu*(x-1+mu)/(r2^3);
    yddot = -2*xdot + y - (1-mu)*(y/(r1^3)) - mu*(y)/(r2^3);
    zddot = -(1-mu)*(z)/(r1^3) - mu*(z)/(r2^3);
    a_CR3BP = [xddot; yddot; zddot];
    
    % Mass Flow Rate
    mdot = 0;
    
    % Random C_R (from predetermined random C_R vector)
    C_R = interp1q(reshape(t_vec,[],1), reshape(C_R_vec,[],1), t);
    
    % SRP
    u_hat = R2/r2;
    a_srp = C_R*Phi/(c*r2^2)*A_sc/m*u_hat;
    
    % Total Acceleration
    a_total = a_CR3BP + a_srp + a_u;

    X_dot = [X(4:6); a_total; mdot; zeros(3,1)];

end