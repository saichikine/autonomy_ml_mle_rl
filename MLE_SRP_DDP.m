function ll_vary = MLE_SRP_DDP(traj,seg_size,C_R_vary,sigma,mu,C_R_vec, t_vec, Phi, A_sc, c_light)
    
    % 1D maximum likelihood estimator for SRP parameters
    
    a_u = NaN(traj.nu,seg_size);
    for k = 1:seg_size
        a_u(:,k) = traj.max_thrust_mag*(traj.stage{k}.u)/traj.stage{k}.state(7);
    end
    
    states = NaN(traj.nx,seg_size);
    for k = 1:seg_size
        states(:,k) = traj.stage{k}.state;
    end
    
    C_R_residuals = NaN(seg_size,1);
    
    for i = 1:seg_size
        X = [states(:,i); traj.stage{i}.u];
        x = X(1);
        y = X(2);
        if x > 1-mu
            if abs(y)<=4.263516523878126e-05
                C_R = 0;
                %a_u = 0*a_u;
            end
        end
        t = t_vec(i);
        a_total_actual = CR3BP_SRP_stoch_cart_control(t,X,mu,traj.exh_vel,traj.max_thrust_mag,C_R_vec,t_vec,Phi,A_sc,c_light);
        Xdot_CR3BP = CR3BP(t,X(1:6),mu);
        a_CR3BP = Xdot_CR3BP(4:6);
        a_SRP_actual = a_total_actual(4:6) - a_CR3BP - a_u(:,i);
        R_sc = X(1:3);
        R_sun = [-mu; 0; 0]; % Assuming Sun is first primary, DOES NOT WORK OTHERWISE
        R2 = R_sc - R_sun;
        r2 = norm(R2);
        C_R_actual = c_light*r2^2/Phi*X(7)/A_sc*norm(a_SRP_actual);
        C_R_residuals(i) = C_R_actual;
    end
    
    % Compute log likelihood
    
    ll_vary = NaN(length(C_R_vary),1);
    
    for cr = 1:length(C_R_vary)
        C_R_hat = C_R_vary(cr);
        l = 0;
        for i = 1:seg_size
            %L_i = log(1/sqrt(2*pi*sigma^2)*exp(-1/2*(C_R_residual_vec(i) - C_R_hat)^2/sigma^2));
            L_i = log(1/sqrt(2*pi*sigma^2)) + (-1/2*(C_R_residuals(i) - C_R_hat)^2/sigma^2);
            l = l + L_i;
        end
        ll_vary(cr) = l;
    end

end