/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fXX_SRP.h
 *
 * Code generation for function 'fXX_SRP'
 *
 */

#ifndef FXX_SRP_H
#define FXX_SRP_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "CR3BP_SRP_cart_control_STMSTT_types.h"

/* Function Declarations */
extern void fXX_SRP(const emlrtStack *sp, const real_T X[1110], real_T c, real_T
                    mu, real_T Tmax, real_T C_R, real_T Phi, real_T A_sc, real_T
                    c_light, real_T fXX[1000]);

#endif

/* End of code generation (fXX_SRP.h) */
