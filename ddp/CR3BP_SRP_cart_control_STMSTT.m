function X_dot = CR3BP_SRP_cart_control_STMSTT(t, X, mu, c, Tmax, C_R, Phi, A_sc, c_light)

    %% Unpack
    
    % Reshape to column vector
    X = reshape(X,length(X),[]);

    % Positions
    x = X(1);
    y = X(2);
    z = X(3);

    % Velocities
    xdot = X(4);
    ydot = X(5);
    zdot = X(6);
    
    % Mass
    m = X(7);
    
    % Control
    ux = X(8);
    uy = X(9);
    uz = X(10);
    
    STM = reshape(X(11:10+10^2),10,[]);
    STT = reshape(X(10^2+11:end),[10 10 10]);
    
    %% Dynamics
    
    % Distances to primaries
    r1 = sqrt((x+mu)^2 + y^2 + z^2);
    r2 = sqrt((x-1+mu)^2 + y^2 + z^2);

    % Accelerations 
    xddot = 2*ydot + x -(1-mu)*((x+mu)/(r1^3)) - mu*(x-1+mu)/(r2^3);
    yddot = -2*xdot + y - (1-mu)*(y/(r1^3)) - mu*(y)/(r2^3);
    zddot = -(1-mu)*(z)/(r1^3) - mu*(z)/(r2^3);
    a_CR3BP = [xddot; yddot; zddot];
    
    % Control
    a_u = [Tmax*(ux/m); Tmax*(uy/m); Tmax*(uz/m)];
    
    % Mass Flow Rate
    u_epsilon = [1e-8; 1e-10; 1e-12; 1e-16];
    mdot = -Tmax*sqrt(ux^2 + uy^2 + uz^2 + u_epsilon(1))/c;
    
    % SRP
    a_srpx = C_R*Phi/(c_light*(r1^3))*A_sc/m*(x+mu);
    a_srpy = C_R*Phi/(c_light*(r1^3))*A_sc/m*(y);
    a_srpz = C_R*Phi/(c_light*(r1^3))*A_sc/m*(z);
    a_srp = [a_srpx; a_srpy; a_srpz];
    
    % Total Acceleration
    a_total = a_CR3BP + a_srp + a_u;
    
    %% STM Dynamics
    
    fXmat = fX_SRP(X, c, mu, Tmax, C_R, Phi, A_sc, c_light);
    STMdot = fXmat*STM;
    
    %% STT Dynamics
    
    fXXten = fXX_SRP(X, c, mu, Tmax, C_R, Phi, A_sc, c_light);

    STTdot = zeros(size(STT));
    temp = zeros(size(STT));
    for i = 1:10
        temp(i,:,:) = STM'*squeeze(fXXten(i,:,:))*STM;
    end
    for i = 1:10
        STTdot(:,:,i) = fXmat*STT(:,:,i) + temp(:,:,i);
    end

    X_dot = [X(4:6); a_total; mdot; zeros(3,1); reshape(STMdot,[],1); reshape((STTdot),[],1)];

end