function traj = generate_traj(num_stages, num_phases)

    nx = 7;
    nu = 3;
    nw = 0;
    nl = 6;
    nX = nx+nu+nw;

    stage_struct = struct('nominal_state',zeros(nx,1),'state',zeros(nx,1),'time',0,'nominal_u',zeros(nu,1),'u',zeros(nu,1),'A',zeros(3,1),'B',zeros(nu,nx),'C',zeros(nu,1),'D',zeros(nu,nl),...
        'JX_star',zeros(nX,1),'Jl_star',zeros(nl,1),'JXX_star',zeros(nX,nX),'Jll_star',zeros(nl,nl),'JXl_star',zeros(nX,nl),'STM',zeros(nX,nX),'STT',zeros([nX nX nX]),'ER',0,'deltax',zeros(nx,1),'deltax_prev',zeros(nx,1));
    
    coder.varsize('stage_struct.A');
    coder.varsize('stage_struct.B');
    coder.varsize('stage_struct.C');
    coder.varsize('stage_struct.D');
    coder.cstructname(stage_struct, 'stage_struct');
    
    stages = repmat(stage_struct,num_stages,1);
    
    traj = struct('stage',stages,'num_stages',num_stages,'num_phases',num_phases,'mu',0,'exh_vel',0,'normalizers',[],'initial_state',zeros(nx,1),'lambda',zeros(nl,1),'nominal_lambda',zeros(nl,1),'w',[],'nx',nx,'nu',nu,'nw',nw,'nl',nl,...
    'stage_times',zeros(num_stages,1),'deltaw',[],'deltal',zeros(nl,1),'ER_phase',0,'J_nom',0,'h_nom',0,'f_nom',0,...
    'max_thrust_mag',0,'eta1',0,'eta2',0,'bool_TRQP_failure',false,...
    'iterate_epsilon',0,'opt_epsilon',0,'feas_epsilon',0,'penalty_sigma',0,'k_sigma',0,...
    'kappa',0,'delta_TRQP',0,'delta_TRQP_min',0,'delta_TRQP_max',0);

    coder.varsize('traj(:).stage.A');
    coder.varsize('traj(:).stage.B');
    coder.varsize('traj(:).stage.C');
    coder.varsize('traj(:).stage.D');

    coder.cstructname(traj, 'traj');

end