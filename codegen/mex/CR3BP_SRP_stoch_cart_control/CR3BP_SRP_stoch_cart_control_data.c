/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * CR3BP_SRP_stoch_cart_control_data.c
 *
 * Code generation for function 'CR3BP_SRP_stoch_cart_control_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "CR3BP_SRP_stoch_cart_control.h"
#include "CR3BP_SRP_stoch_cart_control_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131482U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "CR3BP_SRP_stoch_cart_control",      /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2433290357U, 2237796540U, 4066813863U, 833189415U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo u_emlrtRSI = { 36,         /* lineNo */
  "flip",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elmat\\flip.m"/* pathName */
};

emlrtRSInfo x_emlrtRSI = { 1,          /* lineNo */
  "normrnd",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\private\\normrnd.p"/* pathName */
};

emlrtRTEInfo h_emlrtRTEI = { 128,      /* lineNo */
  19,                                  /* colNo */
  "interp1_work",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pName */
};

emlrtRTEInfo j_emlrtRTEI = { 12,       /* lineNo */
  9,                                   /* colNo */
  "sqrt",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\elfun\\sqrt.m"/* pName */
};

/* End of code generation (CR3BP_SRP_stoch_cart_control_data.c) */
