/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * CR3BP_SRP_stoch_cart_control.c
 *
 * Code generation for function 'CR3BP_SRP_stoch_cart_control'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "CR3BP_SRP_stoch_cart_control.h"
#include "sqrt.h"
#include "eml_int_forloop_overflow_check.h"
#include "CR3BP_SRP_stoch_cart_control_emxutil.h"
#include "randn.h"
#include "CR3BP_SRP_stoch_cart_control_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 28,    /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 29,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 32,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 33,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 34,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 42,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 47,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 49,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 55,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 56,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 57,  /* lineNo */
  "CR3BP_SRP_stoch_cart_control",      /* fcnName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 45,  /* lineNo */
  "mpower",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\mpower.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 55,  /* lineNo */
  "power",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 28,  /* lineNo */
  "reshapeSizeChecks",                 /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\reshapeSizeChecks.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 50,  /* lineNo */
  "interp1",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 162, /* lineNo */
  "interp1_work",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 171, /* lineNo */
  "interp1_work",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 179, /* lineNo */
  "interp1_work",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 182, /* lineNo */
  "interp1_work",                      /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pathName */
};

static emlrtRSInfo t_emlrtRSI = { 21,  /* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"/* pathName */
};

static emlrtRSInfo v_emlrtRSI = { 11,  /* lineNo */
  "normrnd",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\stats\\eml\\normrnd.m"/* pathName */
};

static emlrtRSInfo w_emlrtRSI = { 1,   /* lineNo */
  "rnd",                               /* fcnName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\rnd.p"/* pathName */
};

static emlrtRTEInfo emlrtRTEI = { 47,  /* lineNo */
  19,                                  /* colNo */
  "CR3BP_SRP_stoch_cart_control",      /* fName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 47,/* lineNo */
  40,                                  /* colNo */
  "CR3BP_SRP_stoch_cart_control",      /* fName */
  "C:\\Users\\u99a\\Google Drive\\Grad School\\ASEN 6519 Probabilistic Algorithms for Aerospace Autonomy\\Final Project\\autonomy_ml_mle_r"
  "l\\CR3BP_SRP_stoch_cart_control.m"  /* pName */
};

static emlrtRTEInfo d_emlrtRTEI = { 184,/* lineNo */
  13,                                  /* colNo */
  "interp1_work",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 164,/* lineNo */
  13,                                  /* colNo */
  "interp1_work",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pName */
};

static emlrtRTEInfo f_emlrtRTEI = { 153,/* lineNo */
  15,                                  /* colNo */
  "interp1_work",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pName */
};

static emlrtRTEInfo g_emlrtRTEI = { 137,/* lineNo */
  23,                                  /* colNo */
  "interp1_work",                      /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\lib\\matlab\\polyfun\\interp1.m"/* pName */
};

static emlrtRTEInfo i_emlrtRTEI = { 51,/* lineNo */
  13,                                  /* colNo */
  "reshapeSizeChecks",                 /* fName */
  "C:\\Program Files\\MATLAB\\R2019a\\toolbox\\eml\\eml\\+coder\\+internal\\reshapeSizeChecks.m"/* pName */
};

/* Function Definitions */
void CR3BP_SRP_stoch_cart_control(const emlrtStack *sp, real_T t, const real_T
  X[10], real_T mu, real_T c, real_T Tmax, const emxArray_real_T *C_R_vec, const
  emxArray_real_T *t_vec, real_T Phi, real_T A_sc, real_T c_light, real_T X_dot
  [10])
{
  real_T a_tmp;
  real_T xtmp;
  real_T tmp;
  real_T r1;
  real_T b_a_tmp;
  real_T r2;
  real_T a_u_idx_0;
  real_T a_u_idx_1;
  real_T a_u_idx_2;
  real_T x;
  int32_T nx;
  int32_T maxdimlen;
  int32_T b_nx;
  emxArray_real_T *varargin_1;
  int32_T low_ip1;
  emxArray_real_T *varargin_2;
  int32_T exitg1;
  int32_T nd2;
  real_T C_R;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /* # codegen */
  /*  Reshape to column vector */
  /*  Positions */
  /*  Velocities */
  /*  Mass */
  /*  Control */
  /*  Distances to primaries */
  st.site = &emlrtRSI;
  a_tmp = X[0] + mu;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  xtmp = X[1] * X[1];
  tmp = X[2] * X[2];
  r1 = (a_tmp * a_tmp + xtmp) + tmp;
  st.site = &emlrtRSI;
  b_sqrt(&st, &r1);
  st.site = &b_emlrtRSI;
  b_a_tmp = (X[0] - 1.0) + mu;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &b_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &b_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  r2 = (b_a_tmp * b_a_tmp + xtmp) + tmp;
  st.site = &b_emlrtRSI;
  b_sqrt(&st, &r2);

  /*  Accelerations  */
  st.site = &c_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &c_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &d_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &d_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &e_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &e_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;

  /*  Control */
  a_u_idx_0 = Tmax * (X[7] / X[6]);
  a_u_idx_1 = Tmax * (X[8] / X[6]);
  a_u_idx_2 = Tmax * (X[9] / X[6]);

  /*  Mass Flow Rate */
  st.site = &f_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &f_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &f_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &f_emlrtRSI;
  x = ((X[7] * X[7] + X[8] * X[8]) + X[9] * X[9]) + 1.0E-8;
  if (x < 0.0) {
    emlrtErrorWithMessageIdR2018a(&st, &j_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "sqrt");
  }

  x = muDoubleScalarSqrt(x);

  /*  SRP */
  /*  Random C_R (from predetermined random C_R vector) */
  st.site = &g_emlrtRSI;
  nx = t_vec->size[0] * t_vec->size[1];
  b_st.site = &n_emlrtRSI;
  maxdimlen = t_vec->size[0];
  if (t_vec->size[1] > t_vec->size[0]) {
    maxdimlen = t_vec->size[1];
  }

  maxdimlen = muIntScalarMax_sint32(nx, maxdimlen);
  if (nx > maxdimlen) {
    emlrtErrorWithMessageIdR2018a(&st, &i_emlrtRTEI,
      "Coder:toolbox:reshape_emptyReshapeLimit",
      "Coder:toolbox:reshape_emptyReshapeLimit", 0);
  }

  if (1 > maxdimlen) {
    emlrtErrorWithMessageIdR2018a(&st, &i_emlrtRTEI,
      "Coder:toolbox:reshape_emptyReshapeLimit",
      "Coder:toolbox:reshape_emptyReshapeLimit", 0);
  }

  st.site = &g_emlrtRSI;
  b_nx = C_R_vec->size[0] * C_R_vec->size[1];
  b_st.site = &n_emlrtRSI;
  maxdimlen = C_R_vec->size[0];
  if (C_R_vec->size[1] > C_R_vec->size[0]) {
    maxdimlen = C_R_vec->size[1];
  }

  maxdimlen = muIntScalarMax_sint32(b_nx, maxdimlen);
  if (b_nx > maxdimlen) {
    emlrtErrorWithMessageIdR2018a(&st, &i_emlrtRTEI,
      "Coder:toolbox:reshape_emptyReshapeLimit",
      "Coder:toolbox:reshape_emptyReshapeLimit", 0);
  }

  if (1 > maxdimlen) {
    emlrtErrorWithMessageIdR2018a(&st, &i_emlrtRTEI,
      "Coder:toolbox:reshape_emptyReshapeLimit",
      "Coder:toolbox:reshape_emptyReshapeLimit", 0);
  }

  emxInit_real_T(&st, &varargin_1, 1, &emlrtRTEI, true);
  st.site = &g_emlrtRSI;
  low_ip1 = varargin_1->size[0];
  varargin_1->size[0] = nx;
  emxEnsureCapacity_real_T(&st, varargin_1, low_ip1, &emlrtRTEI);
  for (low_ip1 = 0; low_ip1 < nx; low_ip1++) {
    varargin_1->data[low_ip1] = t_vec->data[low_ip1];
  }

  emxInit_real_T(&st, &varargin_2, 1, &b_emlrtRTEI, true);
  low_ip1 = varargin_2->size[0];
  varargin_2->size[0] = b_nx;
  emxEnsureCapacity_real_T(&st, varargin_2, low_ip1, &b_emlrtRTEI);
  for (low_ip1 = 0; low_ip1 < b_nx; low_ip1++) {
    varargin_2->data[low_ip1] = C_R_vec->data[low_ip1];
  }

  b_st.site = &o_emlrtRSI;
  if (nx != b_nx) {
    emlrtErrorWithMessageIdR2018a(&b_st, &g_emlrtRTEI,
      "Coder:MATLAB:interp1_YInvalidNumRows",
      "Coder:MATLAB:interp1_YInvalidNumRows", 0);
  }

  if (nx <= 1) {
    emlrtErrorWithMessageIdR2018a(&b_st, &f_emlrtRTEI,
      "MATLAB:interp1:NotEnoughPts", "MATLAB:interp1:NotEnoughPts", 0);
  }

  c_st.site = &p_emlrtRSI;
  if (nx > 2147483646) {
    d_st.site = &t_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  maxdimlen = 0;
  do {
    exitg1 = 0;
    if (maxdimlen <= nx - 1) {
      if (muDoubleScalarIsNaN(t_vec->data[maxdimlen])) {
        emlrtErrorWithMessageIdR2018a(&b_st, &e_emlrtRTEI,
          "MATLAB:interp1:NaNinX", "MATLAB:interp1:NaNinX", 0);
      } else {
        maxdimlen++;
      }
    } else {
      if (t_vec->data[1] < t_vec->data[0]) {
        maxdimlen = nx >> 1;
        c_st.site = &q_emlrtRSI;
        for (nd2 = 0; nd2 < maxdimlen; nd2++) {
          xtmp = varargin_1->data[nd2];
          low_ip1 = (nx - nd2) - 1;
          varargin_1->data[nd2] = varargin_1->data[low_ip1];
          varargin_1->data[low_ip1] = xtmp;
        }

        c_st.site = &r_emlrtRSI;
        if ((b_nx != 0) && (b_nx > 1)) {
          nd2 = b_nx >> 1;
          for (maxdimlen = 0; maxdimlen < nd2; maxdimlen++) {
            tmp = varargin_2->data[maxdimlen];
            low_ip1 = (b_nx - maxdimlen) - 1;
            varargin_2->data[maxdimlen] = varargin_2->data[low_ip1];
            varargin_2->data[low_ip1] = tmp;
          }
        }
      }

      c_st.site = &s_emlrtRSI;
      for (maxdimlen = 2; maxdimlen <= nx; maxdimlen++) {
        if (varargin_1->data[maxdimlen - 1] <= varargin_1->data[maxdimlen - 2])
        {
          emlrtErrorWithMessageIdR2018a(&b_st, &d_emlrtRTEI,
            "Coder:toolbox:interp1_nonMonotonicX",
            "Coder:toolbox:interp1_nonMonotonicX", 0);
        }
      }

      C_R = rtNaN;
      if ((!muDoubleScalarIsNaN(t)) && (!(t > varargin_1->data[varargin_1->size
            [0] - 1])) && (!(t < varargin_1->data[0]))) {
        maxdimlen = varargin_1->size[0];
        nd2 = 1;
        low_ip1 = 2;
        while (maxdimlen > low_ip1) {
          nx = (nd2 >> 1) + (maxdimlen >> 1);
          if (((nd2 & 1) == 1) && ((maxdimlen & 1) == 1)) {
            nx++;
          }

          if (t >= varargin_1->data[nx - 1]) {
            nd2 = nx;
            low_ip1 = nx + 1;
          } else {
            maxdimlen = nx;
          }
        }

        if (t >= (varargin_1->data[nd2] + varargin_1->data[nd2 - 1]) / 2.0) {
          maxdimlen = nd2;
        } else {
          maxdimlen = nd2 - 1;
        }

        C_R = varargin_2->data[maxdimlen];
      }

      exitg1 = 1;
    }
  } while (exitg1 == 0);

  emxFree_real_T(&varargin_2);
  emxFree_real_T(&varargin_1);
  if (X[0] > 1.0 - mu) {
    st.site = &h_emlrtRSI;
    b_st.site = &v_emlrtRSI;
    c_st.site = &w_emlrtRSI;
    xtmp = randn();
    xtmp *= 5.0E-5;
    if (muDoubleScalarAbs(X[1]) <= 4.2635165238781263E-5 * (1.0 + xtmp)) {
      C_R = 0.0;
      a_u_idx_0 *= 0.0;
      a_u_idx_1 *= 0.0;
      a_u_idx_2 *= 0.0;
    }
  }

  st.site = &i_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &j_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;
  st.site = &k_emlrtRSI;
  b_st.site = &l_emlrtRSI;
  c_st.site = &m_emlrtRSI;

  /*  Total Acceleration */
  r1 = muDoubleScalarPower(r1, 3.0);
  tmp = muDoubleScalarPower(r2, 3.0);
  xtmp = C_R * Phi / (c_light * r1) * A_sc / X[6];
  X_dot[3] = ((((2.0 * X[4] + X[0]) - (1.0 - mu) * (a_tmp / r1)) - mu * b_a_tmp /
               tmp) + xtmp * a_tmp) + a_u_idx_0;
  X_dot[4] = ((((-2.0 * X[3] + X[1]) - (1.0 - mu) * (X[1] / r1)) - mu * X[1] /
               tmp) + xtmp * X[1]) + a_u_idx_1;
  X_dot[5] = ((-(1.0 - mu) * X[2] / r1 - mu * X[2] / tmp) + xtmp * X[2]) +
    a_u_idx_2;
  X_dot[6] = -Tmax * x / c;
  X_dot[0] = X[3];
  X_dot[7] = 0.0;
  X_dot[1] = X[4];
  X_dot[8] = 0.0;
  X_dot[2] = X[5];
  X_dot[9] = 0.0;
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (CR3BP_SRP_stoch_cart_control.c) */
